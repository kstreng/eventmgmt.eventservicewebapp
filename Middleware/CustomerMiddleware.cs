﻿using EventMgmt.Common.Models;
using Microsoft.AspNetCore.Http;
using System.Linq;
using System.Threading.Tasks;

namespace EventMgmt.EventServiceWebApp.Middleware
{
    public class CustomerMiddleware
    {
        private RequestDelegate _next;

        public CustomerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext, IEventDB store)
        {
            var user = httpContext.Request.Headers["X-Customer"];
            store.CurrentCustomer = user;
            await _next(httpContext);
        }
    }
}
