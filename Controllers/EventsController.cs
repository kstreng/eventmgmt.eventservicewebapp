﻿using System.Collections.Generic;
using EventMgmt.Common.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;

namespace EventMgmt.EventServiceWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventsController : ControllerBase
    {
        public IFileProvider FileProvider { get; }
        public IEventDB EventDB { get; set; }

        public EventsController(IFileProvider fileProvider, IEventDB eventDB)
        {
            FileProvider = fileProvider;
            EventDB = eventDB;
        }

        public IEnumerable<Event> GetEvents()
        {
            return EventDB.LoadEvents();
        }
    }
}