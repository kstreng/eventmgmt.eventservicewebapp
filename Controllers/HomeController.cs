﻿using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using EventMgmt.EventServiceWebApp.Models;
using EventMgmt.Common.Models;

namespace EventMgmt.EventServiceWebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IEventDB _repository;

        public HomeController(ILogger<HomeController> logger, IEventDB repository)
        {
            _logger = logger;
            _repository = repository;
        }

        public IActionResult Index()
        {
            IEnumerable<Event> eventList = _repository.LoadEvents();

            return View(eventList);
        }

        public IActionResult Create(string customer, string name)
        {
            _repository.SaveEvent(new Event(customer, name));

            return RedirectToAction("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
