﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace EventMgmt.EventServiceWebApp.Helpers
{
    public class SimpleTagHelper : TagHelper
    {
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "strong";
        }
    }
}
